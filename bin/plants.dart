import 'package:plants/plants.dart' as plants;

void main(List<String> arguments) {
  Chaba chaba = Chaba('Chaba', '5');
  chaba.introduce();
  chaba.flowerplants();
  chaba.quantitypetals();
  chaba.yPetal();

  Tonson tonson = Tonson('Tonson', '0');
  tonson.introduce();
  tonson.quantitypetals();
  tonson.nPetal();
}

abstract class Plants {
  String? name;
  String? petal;

  Plants(String name, String petal) {
    this.name = name;
    this.petal = petal;
  }

  void introduce();

  void quantitypetals();
}

class Chaba extends Plants with Flowerplants{
  Chaba(String name, String petal) : super(name, petal);
  @override
  void introduce() {
    print('Hello my name is $name');
  }
  @override
  void quantitypetals() {
    print('petal: $petal');
  }

  void yPetal() {
    print('I have petal');
  }
}

class Tonson extends Plants {
  Tonson(String name, String petal) :super(name, petal);
  @override
  void introduce() {
    print('Hello my name is $name');
  }
  @override
  void quantitypetals() {
    print('petal: $petal');
  }

  void nPetal() {
    print('I have no petal');
  }
}

mixin Flowerplants {
  void flowerplants() {
    print('Chaba is flower plant');
  }
}